//
//  AppDelegateRouter.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 13/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import UIKit

protocol AppDelegateRoutingLogic
{
    func routeToMainView()
    func openApp(with url: URL)
}

class AppDelegateRouter : AppDelegateRoutingLogic
{
    weak var appDelegate: AppDelegate?
    
    init(appDelegate: AppDelegate)
    {
        self.appDelegate = appDelegate
    }
    
    func routeToMainView()
    {
        let mainVC = self.getMainViewController()
        self.setAsVisibleView(viewController: mainVC)
    }
    
    func openApp(with url: URL)
    {
        if UIApplication.shared.canOpenURL(url)
        {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    private func getMainViewController() -> UIViewController
    {
        let mainVC = GamesListViewController(nibName: nil, bundle: nil)
        let mainNavC = UINavigationController(rootViewController: mainVC)
        
        return mainNavC
    }
    
    func setAsVisibleView(viewController: UIViewController)
    {
        if self.appDelegate?.window == nil
        {
            self.appDelegate?.window = UIWindow(frame: UIScreen.main.bounds)
        }
        
        self.appDelegate?.window?.rootViewController = viewController
        self.appDelegate?.window?.makeKeyAndVisible()
    }
}
