//
//  ExternalApplication.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 14/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation

protocol ExternalIOSApplicationData
{
    var appURL : URL? { get }
}

struct IOSApplication
{
    let appPath = "://app"
    
    struct Chrome : ExternalIOSApplicationData
    {
        static let name = "googlechrome"
        static let externalAppPath = name + "://"
        
        var appURL : URL? = URL(string: externalAppPath)
    }
}
