//
//  QueueNames.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation

struct QueueNames
{
    static let responseQueue = "com.asalvador.21ButtonsTest"
}
