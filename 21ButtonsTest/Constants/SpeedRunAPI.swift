//
//  SpeedRunAPI.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation

struct SpeedRunAPI
{
    static let APIBaseURL   = "http://www.speedrun.com/api"
    static let APIVersion1  = "v1"
    
    static let APIPathV1    = APIBaseURL + "/" + APIVersion1
    
    struct Endpoint
    {
        static let games    = "/games"
        static let users    = "/users"
        static let runs     = "/runs"
    }
}
