//
//  ErrorProtocol.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation

protocol ErrorProtocol : Error
{
    var errorMessage : String { get }
    var errorCode : Int { get }
}
