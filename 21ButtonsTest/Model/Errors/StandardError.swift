//
//  StandardErrors.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation

enum StandardError : ErrorProtocol
{
    case unknown
    case couldNotObtainGame
    case couldNotObtainUser
    
    var errorMessage: String {
        
        switch self
        {
        case .unknown:              return L10n.Errors.unknown
        case .couldNotObtainGame:   return "Could not obtain game"
        case .couldNotObtainUser:   return "Could not obtain user"
        }
    }
    
    var errorCode: Int {
        return 0
    }
}
