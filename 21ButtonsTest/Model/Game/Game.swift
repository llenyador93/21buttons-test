//
//  Game.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 08/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import ObjectMapper

struct Game : Mappable
{
    var id              : String?
    var names           : Names?
    var abbreviation    : String?
    var weblink         : String?
    var releaseDate     : String?
    var created         : String?
    var assets          : GameAssets?
    var links           : [Link]?
    
    init?(map: Map) {}
    
    init() {}
    
    mutating func mapping(map: Map)
    {
        self.id             <- map["id"]
        self.names          <- map["names"]
        self.abbreviation   <- map["abbreviation"]
        self.weblink        <- map["weblink"]
        self.releaseDate    <- map["release-date"]
        self.created        <- map["created"]
        self.assets         <- map["assets"]
        self.links          <- map["links"]
    }
    
    var parameters : [String : Any] {
        
        var params : [String : Any] = [:]
        
        if let gameId = self.id
        {
            params["game"] = gameId
        }
        
        return params
    }
}
