//
//  GameAsset.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 08/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import ObjectMapper

struct GameAsset : Mappable
{
    var uri     : String?
    var width   : Int?
    var height  : Int?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        self.uri    <- map["uri"]
        self.width  <- map["width"]
        self.height <- map["height"]
    }
    
    var size : CGSize? {
    
        guard let height = self.height, let width = self.width else { return nil }
        return CGSize(width: width, height: height)
    }
}
