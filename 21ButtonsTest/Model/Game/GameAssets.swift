//
//  GameAsset.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 08/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import ObjectMapper

struct GameAssets : Mappable
{
    var logo        : GameAsset?
    var coveTiny    : GameAsset?
    var coverSmall  : GameAsset?
    var coverMedium : GameAsset?
    var coverLarge  : GameAsset?
    var icon        : GameAsset?
    var trophy1st   : GameAsset?
    var trophy2nd   : GameAsset?
    var trophy3rd   : GameAsset?
    var trophy4th   : GameAsset?
    var background  : GameAsset?
    var foreground  : GameAsset?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        self.logo        <- map["logo"]
        self.coveTiny    <- map["cover-tiny"]
        self.coverSmall  <- map["cover-small"]
        self.coverMedium <- map["cover-medium"]
        self.coverLarge  <- map["cover-large"]
        self.icon        <- map["icon"]
        self.trophy1st   <- map["trophy-1st"]
        self.trophy2nd   <- map["trophy-2nd"]
        self.trophy3rd   <- map["trophy-3rd"]
        self.trophy4th   <- map["trophy-4th"]
        self.background  <- map["background"]
        self.foreground  <- map["foreground"] 
    }
}
