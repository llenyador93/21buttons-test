//
//  GamesList.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import ObjectMapper

struct GamesList : Mappable
{
    var data        : [Game]?
    var pagination  : Pagination?
    
    init?(map: Map) {}
    
    init() {}
    
    mutating func mapping(map: Map)
    {
        self.data       <- map["data"]
        self.pagination <- map["pagination"]
    }
}
