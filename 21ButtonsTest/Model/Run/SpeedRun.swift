//
//  SpeedRun.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 13/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import ObjectMapper

struct SpeedRun : Mappable
{
    var id          : String?
    var gameId      : String?
    var videos      : SpeedRunVideoLinks?
    var players     : [PlayerPreview]?
    var date        : String?
    var times       : SpeedRunTimes?
    var links       : String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        self.id         <- map["id"]
        self.gameId     <- map["game"]
        self.videos     <- map["videos"]
        self.players    <- map["players"]
        self.date       <- map["date"]
        self.times      <- map["times"]
        self.links      <- map["links"]
    }
}
