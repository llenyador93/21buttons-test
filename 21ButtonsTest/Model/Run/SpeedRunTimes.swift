//
//  SpeedRunTimes.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 13/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import ObjectMapper

struct SpeedRunTimes : Mappable
{
    var primaryTimeTimestamp    : Int?
    var realtimeTimestamp       : Int?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        self.primaryTimeTimestamp   <- map["primary_t"]
        self.realtimeTimestamp      <- map["realtime_t"]
    }
}
