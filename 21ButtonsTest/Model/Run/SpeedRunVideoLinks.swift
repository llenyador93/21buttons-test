//
//  SpeedRunVideoLinks.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 13/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import ObjectMapper

struct SpeedRunVideoLinks : Mappable
{
    var links : [Link]?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        self.links   <- map["links"]
    }
}
