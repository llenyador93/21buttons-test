//
//  Link.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 08/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import ObjectMapper

struct Link : Mappable
{
    var uri : String?
    var rel : String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        self.uri <- map["uri"]
        self.rel <- map["rel"]
    }
}
