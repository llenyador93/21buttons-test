//
//  Names.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 08/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import ObjectMapper

struct Names : Mappable
{
    var international   : String?
    var japanese        : String?
    var twitch          : String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        self.international  <- map["international"]
        self.japanese       <- map["japanese"]
        self.twitch         <- map["twitch"]
    }
}
