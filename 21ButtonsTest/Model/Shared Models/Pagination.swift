//
//  Pagination.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import ObjectMapper

struct Pagination : Mappable
{
    var offset  : Int?
    var max     : Int?
    var size    : Int?
    var links   : [Link]?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        self.offset     <- map["offset"]
        self.max        <- map["max"]
        self.size       <- map["size"]
        self.links      <- map["links"]
    }
}
