//
//  RuleSet.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 08/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import ObjectMapper

struct RuleSet : Mappable
{
    var showMilliseconds    : Bool?
    var requireVerification : Bool?
    var requireVideo        : Bool?
    var runTimes            : [String]?
    var defaultTime         : String?
    var emulatorsAllowed    : Bool?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        self.showMilliseconds    <- map["show-milliseconds"]
        self.requireVerification <- map["require-verification"]
        self.requireVideo        <- map["require-video"]
        self.runTimes            <- map["run-times"]
        self.defaultTime         <- map["default-time"]
        self.emulatorsAllowed    <- map["emulators-allowed"]
    }
}
