//
//  User.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 13/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import ObjectMapper

struct User : Mappable
{
    var id      : String?
    var role    : String?
    var names   : Names?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        self.id     <- map["id"]
        self.role   <- map["role"]
        self.names  <- map["names"]
    }
}
