//
//  UserPreview.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 13/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import ObjectMapper

struct PlayerPreview : Mappable
{
    var rel : String?
    var id  : String?
    var uri : String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        self.rel    <- map["rel"]
        self.id     <- map["id"]
        self.uri    <- map["uri"]
    }
}
