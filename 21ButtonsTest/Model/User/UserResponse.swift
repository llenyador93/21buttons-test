//
//  UserResponse.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 14/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import ObjectMapper

struct UserResponse : Mappable
{
    var data : User?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map)
    {
        self.data <- map["data"]
    }
}
