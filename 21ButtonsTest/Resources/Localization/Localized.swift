// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// swiftlint:disable explicit_type_interface identifier_name line_length nesting type_body_length type_name
internal enum L10n {
  internal enum Localizable {
    /// Error
    internal static let errorTitle = L10n.tr("Localizable", "error_title")
    /// Games
    internal static let games = L10n.tr("Localizable", "games")
    /// Game's title:
    internal static let gamesTitle = L10n.tr("Localizable", "games_title")
    /// Player name:
    internal static let playerName = L10n.tr("Localizable", "player_name")
    /// SpeedRun time:
    internal static let speedrunTime = L10n.tr("Localizable", "speedrun_time")
    /// Video unavailable
    internal static let videoUnavailable = L10n.tr("Localizable", "video_unavailable")
    /// View video
    internal static let viewVideo = L10n.tr("Localizable", "view_video")
  }
  internal enum Errors {
    /// Unknown error
    internal static let unknown = L10n.tr("Errors", "unknown")
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length nesting type_body_length type_name

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
