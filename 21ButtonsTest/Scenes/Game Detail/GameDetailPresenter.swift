//
//  GameDetailPresenter.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 13/01/2019.
//  Copyright (c) 2019 Aitor Salvador. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

fileprivate let kMaxImageSize = CGSize(width: 150, height: 150)

protocol GameDetailPresentationLogic : ErrorPresentable
{
    func present(detail: SpeedRunDetailModel)
    func presentLoadingInfo(loading: Bool)
    func present(image: UIImage?)
}

class GameDetailPresenter: GameDetailPresentationLogic
{
    weak var viewController: GameDetailDisplayLogic?
    
    func present(detail: SpeedRunDetailModel)
    {
        let viewModel = detail.detailViewModel(with: kMaxImageSize)
        
        self.viewController?.display(viewModel: viewModel)
    }
    
    func present(image: UIImage?)
    {
        self.viewController?.display(gameLogo: image ?? Asset.grayBackground.image)
    }
    
    func presentLoadingInfo(loading: Bool)
    {
        loading ? self.viewController?.displayLoadingInfo() : self.viewController?.hideLoadingInfo()
    }
    
    func present(error: Error)
    {
        self.present(error: error, on: self.viewController!)
    }
}
