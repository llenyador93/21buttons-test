//
//  GamesListDataSource.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 12/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import UIKit
import SDWebImage

fileprivate let kMinimumNumberOfRows = 1

struct GamesListDataSource : TableViewDataSource
{
    var games : [Game]              = []
    var isDownloadLastGamesRquestInProcess  = false
    
    func game(at indexPath: IndexPath) -> Game?
    {
        guard self.games.isValid(index: indexPath.row) else { return nil }
        return self.games[indexPath.row]
    }
    
    var numberOfRows : Int {
        
        return max(self.games.count, kMinimumNumberOfRows)
    }
    
    func cell(on tableView: UITableView, at indexPath: IndexPath, delegate: Any) -> UITableViewCell
    {
        if self.games.count > 0
        {
            return self.getGameCell(on: tableView, at: indexPath, delegate: delegate)
        }
        else
        {
            if self.isDownloadLastGamesRquestInProcess
            {
                return self.getLoadingInfoCell(on: tableView, at: indexPath, delegate: delegate, isAnimating: true)
            }
            else
            {
                let viewModel = NoResultsTableViewCellViewModel(message: "No results",
                                                                alignment: .center,
                                                                textColor: UIColor.Palette.midnightBlue)
                return self.getNoResultsCell(on: tableView, at: indexPath, viewModel: viewModel)
            }
        }
    }
    
    func getGameCell(on tableView: UITableView, at indexPath: IndexPath, delegate: Any) -> UITableViewCell
    {
        guard self.games.isValid(index: indexPath.row) else { return UITableViewCell() }
        
        let game = self.games[indexPath.row]
        
        let size = game.assets?.coverLarge?.size
        let titleName = game.names?.international
        let viewModel = GameTableViewCellViewModel(imageSize: size,
                                                           title: titleName)
        let cell = tableView.dequeueReusableCell(withIdentifier: GameTableViewCell.reuseIdentifier) as! GameTableViewCellDisplayLogic
        
        cell.display(viewModel: viewModel, indexPath: indexPath)
        self.displayImage(with: game.assets?.coverLarge?.uri, on: cell, at: indexPath)
        
        return cell as! UITableViewCell
    }
    
    private func displayImage(with url: String?, on cell: GameTableViewCellDisplayLogic, at indexPath: IndexPath)
    {
        SDWebImageDownloader.shared().downloadImage(with: url?.toURL, options: [], progress: nil) { (image, data, error, success) in
     
            guard let cellIndexPath = cell.indexPath, let image = image  else { return }
            
            let isCellIndexPathStillAlive = cellIndexPath == indexPath
            if isCellIndexPathStillAlive
            {
                cell.display(image: image)
            }
        }
    }
}
