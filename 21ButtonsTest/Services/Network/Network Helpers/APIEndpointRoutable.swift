//
//  APIRoutable.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation

protocol APIEndpointRoutable
{
    typealias Parameters = [String : Any]
    
    var route       : String    { get set }
    var urlParams   : String!   { get set }
    
    init()
}
