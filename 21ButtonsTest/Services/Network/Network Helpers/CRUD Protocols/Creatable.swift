//
//  Creatable.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation

// MARK: - Create
protocol Creatable : APIEndpointRoutable {}

extension Creatable where Self : APIEndpointRoutable
{
    static func createURLRequest(parameters: Parameters = [:]) -> URLRequestConverter
    {
        let initialRoute = Self.init()
        let route = initialRoute.route
        
        return URLRequestConverter(method: .post, route: route, parameters: parameters)
    }
    
    static func postURLRequest(nestedRoute: String?, body parameters: Parameters = [:]) -> URLRequestConverter
    {
        let initialRoute = Self.init()
        var route = initialRoute.route
        
        if let nestedRoute = nestedRoute
        {
            route = initialRoute.route + nestedRoute
        }
        
        return URLRequestConverter(method: .post, route: route, parameters: parameters)
    }
}
