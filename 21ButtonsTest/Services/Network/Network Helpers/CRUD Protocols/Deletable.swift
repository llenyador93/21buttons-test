//
//  Deletable.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation

// MARK: - Delete
protocol Deletable : APIEndpointRoutable  {}

extension Deletable where Self : APIEndpointRoutable
{
    static func delete(nestedRoute: String) -> URLRequestConverter {
        
        let temp = Self.init()
        let route = temp.route + nestedRoute
        
        return URLRequestConverter(method: .delete, route: route)
    }
}
