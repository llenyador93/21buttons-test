//
//  Readable.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation

// MARK: - Read
protocol Readable : APIEndpointRoutable  {}

extension Readable where Self : APIEndpointRoutable
{
    static func getURLRequest(nestedRoute: String?, params: Parameters?) -> URLRequestConverter
    {
        let temp = Self.init()
        var tempRoute = "\(temp.route)"
        
        if let nestedRoute = nestedRoute
        {
            tempRoute = temp.route + nestedRoute
        }
        
        if let dictParams = params
        {
            return URLRequestConverter(method: .get,
                                    route: tempRoute,
                                    parameters: dictParams)
        }
        else
        {
            return URLRequestConverter(method: .get,
                                    route: tempRoute)
        }
    }
}
