//
//  Updatable.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation

// MARK: - Update
protocol Updatable : APIEndpointRoutable  {}

extension Updatable where Self : APIEndpointRoutable
{
    static func putURLRequest(nestedRoute: String?, parameters: Parameters?) -> URLRequestConverter
    {
        let temp = Self.init()
        var tempRoute = "\(temp.route)"
        
        if let nestedRoute = nestedRoute
        {
            tempRoute = temp.route + nestedRoute
        }
        
        if let dictParams = parameters
        {
            return URLRequestConverter(method: .put,
                                    route: tempRoute,
                                    parameters: dictParams)
        }
        else
        {
            return URLRequestConverter(method: .put,
                                    route: tempRoute)
        }
    }
}











