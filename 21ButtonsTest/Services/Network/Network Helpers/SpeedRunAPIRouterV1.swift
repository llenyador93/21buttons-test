//
//  APIRouter.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation

struct SpeedRunAPIRouterV1 : URLRoutable
{
    static var basePath: String
    {
        return SpeedRunAPI.APIPathV1
    }
}

extension SpeedRunAPIRouterV1
{
    struct Games : APIEndpointRoutable, Readable
    {
        var route       : String = basePath + SpeedRunAPI.Endpoint.games
        var urlParams   : String!
    }
    
    struct Users : APIEndpointRoutable, Readable
    {
        var route       : String = basePath + SpeedRunAPI.Endpoint.users
        var urlParams   : String!
    }
    
    struct Runs : APIEndpointRoutable, Readable
    {
        var route       : String = basePath + SpeedRunAPI.Endpoint.runs
        var urlParams   : String!
    }
}
