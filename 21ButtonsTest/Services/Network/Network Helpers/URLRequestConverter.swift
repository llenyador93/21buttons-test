//
//  URLRequestConverter.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import Alamofire

protocol ToURLRequestConvertible : URLRequestConvertible
{
    var method      : HTTPMethod    { get set }
    var route       : String        { get set }
    var parameters  : Parameters    { get set }
}

struct URLRequestConverter : ToURLRequestConvertible
{
    var method      : HTTPMethod
    var route       : String
    var parameters  : Parameters
    var useToken    : Bool = true
    
    init(method: HTTPMethod, route: String, parameters: Parameters = [:], useToken: Bool = true)
    {
        self.method     = method
        self.route      = route
        self.parameters = parameters
        self.useToken   = useToken
    }
    
    func asURLRequest() throws -> URLRequest
    {
        let url = try self.route.asURL()
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = self.method.rawValue
        urlRequest.cachePolicy = .reloadIgnoringCacheData
        urlRequest.httpShouldHandleCookies = false
        
        if self.useToken
        {
            // TODO: Add stored token
        }
        
        let isGETMethod = self.method == .get
        if isGETMethod
        {
            return try URLEncoding.default.encode(urlRequest, with: self.parameters)
        }
        else
        {
            return try JSONEncoding.default.encode(urlRequest, with: self.parameters)
        }
    }
}
