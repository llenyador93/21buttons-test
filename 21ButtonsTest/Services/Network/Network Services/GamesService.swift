//
//  GamesService.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import RxSwift

struct GamesService : Serviceable
{
    var appServiceComponent: AppServiceComponent = AppServiceComponent()
    
    func getLastGames() -> Single<GamesList>
    {
        return Single.create(subscribe: { (single) -> Disposable in
            
            let gamesURL = SpeedRunAPIRouterV1.Games.getURLRequest(nestedRoute: nil, params: nil)
            
            self.network.performRequest(urlRequest: gamesURL, objectType: GamesList.self, onComplete: { (response) in
                
                switch response
                {
                case .success(let gamesList):
                    
                    single(.success(gamesList))
                    
                case .failure(let error):
                    
                    single(.error(error))
                }
                
            })
            
            return Disposables.create()
        })
    }
}
