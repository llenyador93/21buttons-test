//
//  ImageService.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 13/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import UIKit

struct ImageService : Serviceable
{
    var appServiceComponent: AppServiceComponent = AppServiceComponent()
    
    func getImage(with key: String?, fromNetworkCompletionHandler: @escaping ((UIImage?) -> ()) ) -> UIImage?
    {
        if let cachedImage = self.storage.fetchImageFromDiskCache(with: key)
        {
            return cachedImage
        }
        else
        {
            self.network.downloadImageFromNetwork(withURL: key?.toURL, completion: fromNetworkCompletionHandler)
            return nil
        }
    }
}
