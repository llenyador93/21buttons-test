//
//  RunsService.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 13/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import RxSwift

struct RunsService : Serviceable
{
    var appServiceComponent: AppServiceComponent = AppServiceComponent()
    
    func getRuns(parameters: [String : Any]) -> Single<SpeedRunsList>
    {
        return Single.create(subscribe: { (single) -> Disposable in
            
            let runsURL = SpeedRunAPIRouterV1.Runs.getURLRequest(nestedRoute: nil, params: parameters)
            
            self.network.performRequest(urlRequest: runsURL, objectType: SpeedRunsList.self, onComplete: { (response) in
                
                switch response
                {
                case .success(let runsList):
                    
                    single(.success(runsList))
                    
                case .failure(let error):
                    
                    single(.error(error))
                }
                
            })
            
            return Disposables.create()
        })
    }
}
