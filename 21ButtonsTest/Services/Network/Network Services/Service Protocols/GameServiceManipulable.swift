//
//  GameServiceManipulable.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import RxSwift

protocol GameServiceManipulable
{
    func getLastGames() -> Single<GamesList>
}

extension GameServiceManipulable
{
    func getLastGames() -> Single<GamesList>
    {
        let gamesService = GamesService()
        return gamesService.getLastGames()
    }
}
