//
//  ImageDownloadable.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 13/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import UIKit

protocol ImageDownloadable
{
    func getImage(withKey key: String?, fromNetworkCompletionHandler: @escaping ( (UIImage?) -> ())) -> UIImage?
    func cache(image: UIImage, key: String?)
}

extension ImageDownloadable
{
    func getImage(withKey key: String?, fromNetworkCompletionHandler: @escaping ( (UIImage?) -> ())) -> UIImage?
    {
        let imageService = ImageService()
        return imageService.getImage(with: key, fromNetworkCompletionHandler: fromNetworkCompletionHandler)
    }
    
    func cache(image: UIImage, key: String?)
    {
        let imageCacheService = ImageCacheService()
        imageCacheService.cacheOnDisk(image: image, withKey: key)
    }
}
