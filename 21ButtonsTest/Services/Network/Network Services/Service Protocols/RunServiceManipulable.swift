//
//  RunServiceManipulable.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 13/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import RxSwift

protocol RunServiceManipulable
{
    func getRuns(parameters: [String : Any]) -> Single<SpeedRunsList>
}

extension RunServiceManipulable
{
    func getRuns(parameters: [String : Any]) -> Single<SpeedRunsList>
    {
        let runsService = RunsService()
        return runsService.getRuns(parameters: parameters)
    }
}
