//
//  PlayerManipulable.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 13/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import RxSwift

protocol UserServiceManipulable
{
    func getUser(withId userId: String) -> Single<User>
}

extension UserServiceManipulable
{
    func getUser(withId userId: String) -> Single<User>
    {
        let userService = UserService()
        return userService.getUser(withId: userId)
    }
}
