//
//  UserService.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 13/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import RxSwift

struct UserService : Serviceable
{
    var appServiceComponent: AppServiceComponent = AppServiceComponent()
    
    func getUser(withId userId: String) -> Single<User>
    {
        return Single.create(subscribe: { (single) -> Disposable in
            
            let nestedRoute = "/" + userId
            let userURL = SpeedRunAPIRouterV1.Users.getURLRequest(nestedRoute: nestedRoute, params: nil)
            
            self.network.performRequest(urlRequest: userURL, objectType: UserResponse.self, onComplete: { (response) in
                
                switch response
                {
                case .success(let userResponse):
                    
                    if let user = userResponse.data
                    {
                        single(.success(user))
                    }
                    else
                    {
                        single(.error(StandardError.unknown))
                    }
                    
                case .failure(let error):
                    
                    single(.error(error))
                }
            })
            
            return Disposables.create()
        })
    }

}
