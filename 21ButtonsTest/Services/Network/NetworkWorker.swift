//
//  NetworkWorker.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import SystemConfiguration
import Alamofire
import SDWebImage
import AlamofireObjectMapper
import ObjectMapper

typealias ResultCompletionHandler = (_ result : Result<Any>) -> Void

protocol NetworkWorkingLogic
{
    func performRequest<T : BaseMappable>(urlRequest: URLRequestConvertible, objectType: T.Type, onComplete completion: @escaping ((_ result : Result<T>) -> Void) )
    func downloadImageFromNetwork(withURL url: URL?, completion: @escaping (UIImage?) -> Void)
}

class NetworkWorker
{
    public static var isConnectionAvailable : Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
}

extension NetworkWorker : NetworkWorkingLogic
{
    func performRequest<T : BaseMappable>(urlRequest: URLRequestConvertible, objectType: T.Type, onComplete completion: @escaping ((_ result : Result<T>) -> Void) )
    {
        let responseQueue = DispatchQueue(label: QueueNames.responseQueue, qos: .utility, attributes: [.concurrent])
        
        Alamofire.request(urlRequest).responseObject(queue: responseQueue) { (response: DataResponse<T>) in
            
            self.readResponse(response: response, completion: completion)
        }
    }
    
    func readResponse<T : BaseMappable>(response : DataResponse<T>, completion: @escaping ((_ result : Result<T>) -> Void) )
    {
        switch response.result
        {
        case .success(let value):
            
            completion(.success(value))
            
        case .failure(let error):
            
            completion(.failure(error))
        }
    }
}

// MARK: - Images
extension NetworkWorker
{
    func downloadImageFromNetwork(withURL url: URL?, completion: @escaping (UIImage?) -> Void)
    {
        SDWebImageDownloader.shared()
        .downloadImage(with: url, options: [], progress: nil) { (image, data, error, success) in
            
            completion(image)
        }
    }
}
