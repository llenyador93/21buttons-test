//
//  ServiceComponent.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation

protocol AppServiceComponentOwner
{
    var appServiceComponent : AppServiceComponent { get set }
}

struct AppServiceComponent
{
    var network : NetworkWorkingLogic = NetworkWorker()
    var storage : StorageWorkingLogic = StorageWorker()
}
