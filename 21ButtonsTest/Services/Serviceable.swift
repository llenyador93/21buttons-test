//
//  Serviceable.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation

protocol Serviceable : AppServiceComponentOwner
{
    var network     : NetworkWorkingLogic       { get set }
    var storage     : StorageWorkingLogic       { get set }
}

extension Serviceable
{
    var network : NetworkWorkingLogic {
        
        get { return self.appServiceComponent.network }
        set { appServiceComponent.network = newValue }
    }
    
    var storage     : StorageWorkingLogic {
        
        get { return self.appServiceComponent.storage }
        set { appServiceComponent.storage = newValue }
    }
}
