//
//  ImageCacheService.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 13/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import SDWebImage

struct ImageCacheService : Serviceable
{
    var appServiceComponent: AppServiceComponent = AppServiceComponent()
    
    func cacheOnDisk(image: UIImage, withKey key: String?)
    {
        self.storage.cache(image: image, withKey: key, toDisk: true)
    }
    
    func fetchImageFromDiskCache(withKey key: String) -> UIImage?
    {
        return self.storage.fetchImageFromDiskCache(with: key)
    }
}
