//
//  StorageWorker.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 13/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import UIKit
import SDWebImage

protocol StorageWorkingLogic
{
    func cache(image: UIImage, withKey key: String?, toDisk: Bool)
    func fetchImageFromDiskCache(with key: String?) -> UIImage?
}

class StorageWorker : StorageWorkingLogic
{
    func cache(image: UIImage, withKey key: String?, toDisk: Bool)
    {
        SDImageCache.shared().store(image, forKey: key, toDisk: toDisk, completion: nil)
    }
    
    func fetchImageFromDiskCache(with key: String?) -> UIImage?
    {
        return SDImageCache.shared().imageFromDiskCache(forKey: key)
    }
}
