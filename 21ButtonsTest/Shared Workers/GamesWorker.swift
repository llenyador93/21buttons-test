//
//  GamesWorker.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import RxSwift

protocol GamesWorkingLogic
{
    func downloadLastGamesList() -> Single<[Game]>
}

class GamesWorker : GamesWorkingLogic, GameServiceManipulable
{
    func downloadLastGamesList() -> Single<[Game]>
    {
        return self.getLastGames().map({ (gamesList) -> [Game] in
            
            return gamesList.data ?? []
        })
    }
}
