//
//  ImagesWorker.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 13/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import UIKit

protocol ImagesWorkingLogic
{
    func getImage(with key: String, fromNetworkCompletionHandler: @escaping ( (UIImage?) -> () ) ) -> UIImage?
}

class ImagesWorker : ImagesWorkingLogic, ImageDownloadable
{
    func getImage(with key: String, fromNetworkCompletionHandler: @escaping ( (UIImage?) -> () ) ) -> UIImage?
    {
        return self.getImage(withKey: key) { (downloadedImage) in
            
            if let image = downloadedImage
            {
                self.cache(image: image, key: key)
            }
            
            fromNetworkCompletionHandler(downloadedImage)
        }
    }
}
