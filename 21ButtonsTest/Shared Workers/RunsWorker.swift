//
//  RunsWorker.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 13/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import RxSwift

protocol RunsWorkingLogic
{
    func getRunDetail(ofGame game: Game?) -> Single<SpeedRunDetailModel>
}

class RunsWorker : RunsWorkingLogic, RunServiceManipulable, UserServiceManipulable
{
    func getRunDetail(ofGame game: Game?) -> Single<SpeedRunDetailModel>
    {
        guard game?.id != nil else { return Single.error(StandardError.couldNotObtainGame) }
        
        var speedRunDetail = SpeedRunDetailModel(game: game!)
        
        return self.getRuns(parameters: game!.parameters).flatMap({ (runsList) -> Single<SpeedRunDetailModel> in
            
            let run = runsList.data?.first
            let player = run?.players?.first
            
            guard let userId = player?.id else { return Single.error(StandardError.couldNotObtainUser) }
            
            speedRunDetail.run = run
            
            return self.getUser(withId: userId).flatMap({ (user) -> Single<SpeedRunDetailModel> in
                
                speedRunDetail.user = user
                return Single.just(speedRunDetail)
            })
        })
    }
}
