//
//  ErrorDisplayable.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import UIKit

protocol ErrorDisplayable : class
{
    func display(errorMessage: String)
}

extension ErrorDisplayable where Self : UIViewController
{
    func display(errorMessage: String)
    {
        self.showErrorAlert(with: errorMessage)
    }
}
