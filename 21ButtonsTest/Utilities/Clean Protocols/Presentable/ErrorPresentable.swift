//
//  ErrorPresentable.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation

protocol ErrorPresentable
{
    func present(error: Error)
}

extension ErrorPresentable
{
    internal func present(error: Error, on viewController: ErrorDisplayable)
    {
        let errorProtocol = self.parse(error: error)
        viewController.display(errorMessage: errorProtocol.errorMessage)
    }
    
    private func parse(error: Error) -> ErrorProtocol
    {
        if let errorProtocol = error as? ErrorProtocol
        {
            return errorProtocol
        }
        else
        {
            return StandardError.unknown
        }
    }
}
