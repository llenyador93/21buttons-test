//
//  URLRoutable.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 14/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import UIKit

protocol ExternalURLRoutable
{
    func routeToSafari(urlToOpen: URL)
    func routeToChrome(urlToOpen: URL)
    
    func route(to url: URL)
}

extension ExternalURLRoutable
{
    func routeToSafari(urlToOpen: URL)
    {
        self.route(to: urlToOpen)
    }
    
    func routeToChrome(urlToOpen: URL)
    {
        guard   var chromeURL = IOSApplication.Chrome().appURL,
                let resourceSpecifier = (urlToOpen as NSURL).resourceSpecifier
        else { return }
        
        chromeURL.appendPathComponent(resourceSpecifier)
        
        self.route(to: chromeURL)
    }
    
    func route(to url: URL)
    {
        (UIApplication.shared.delegate as? AppDelegate)?.router?.openApp(with: url)
    }
}
