//
//  NibIdentifable.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 14/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import UIKit

protocol NibIdentifiable
{
    static var nibNameIdentifier: String { get }
}

// MARK: - Indentifies each storyboard from its classname.
extension NibIdentifiable where Self: UIViewController
{
    static var nibNameIdentifier: String {
        return String(describing: self)
    }
    
    static func instantiateFromNib() -> Self
    {
        return Self(nibName: Self.nibNameIdentifier, bundle:nil)
    }
    
    static func instantiateNavigationControllerFromNib() -> UINavigationController
    {
        let controller = Self(nibName: Self.nibNameIdentifier, bundle:nil)
        let navController = UINavigationController(rootViewController: controller)
        return navController
    }
}

extension UIViewController : NibIdentifiable {}
