//
//  Reusable.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation

import UIKit

protocol Reusable: class
{
    static var reuseIdentifier: String { get }
    static var nib: UINib? { get }
}

extension Reusable
{
    static var reuseIdentifier: String { return String(describing: self) }
    static var nib: UINib? { return nil }
}
