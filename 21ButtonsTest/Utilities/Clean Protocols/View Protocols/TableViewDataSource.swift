//
//  TableViewDataSource.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 10/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import UIKit

protocol TableViewDataSource {}

extension TableViewDataSource
{
    public func getLoadingInfoCell(on tableView: UITableView, at indexPath: IndexPath, delegate: Any, isAnimating: Bool) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: LoadingTableViewCell.reuseIdentifier) as! LoadingTableViewCell
        cell.display(animating: true, activityIndicatorColor: nil)
        return cell
    }
    
    func getNoResultsCell(on tableView: UITableView, at indexPath: IndexPath, viewModel: NoResultsTableViewCellViewModel) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: NoResultsTableViewCell.reuseIdentifier) as! NoResultsTableViewCellDisplayLogic
        cell.display(viewModel: viewModel)
        return cell as! NoResultsTableViewCell
    }
}
