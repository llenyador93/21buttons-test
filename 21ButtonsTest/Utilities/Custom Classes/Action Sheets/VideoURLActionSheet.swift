//
//  VideoURLActionSheet.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 14/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import UIKit

typealias ActionSheetDelegate = UIImagePickerControllerDelegate & UINavigationControllerDelegate


protocol VideoURLActionSheetDelegate : class
{
    func didSelectSafariOption(alert: UIAlertAction)
    func didSelectChromeOption(alert: UIAlertAction)
}

struct VideoURLActionSheet
{
    public static func externalVideoSheet(from sourceView: UIView, on delegate: VideoURLActionSheetDelegate) -> UIAlertController
    {
        let alertController = UIAlertController(title: nil, message: "Choose option", preferredStyle: .actionSheet)
        
        let safariOption = UIAlertAction(title: "Open in safari", style: .default, handler: delegate.didSelectSafariOption)
        alertController.addAction(safariOption)
        
        if ApplicationInstalledChecker.isChromeInstalled
        {
            let chromeOption = UIAlertAction(title: "Open in chrome", style: .default, handler: delegate.didSelectChromeOption)
            alertController.addAction(chromeOption)
        }
        
        let cancelOption = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelOption)
        
        return alertController
    }
}
