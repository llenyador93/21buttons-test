//
//  ApplicationInstalledChecker.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 14/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import UIKit

class ApplicationInstalledChecker
{
    static var isChromeInstalled : Bool {
        
        return canOpenApp(app: IOSApplication.Chrome())
    }
    
    static func canOpenApp(app: ExternalIOSApplicationData) -> Bool
    {
        guard let url = app.appURL else { return false }
        return UIApplication.shared.canOpenURL(url)
    }
}
