//
//  Array + Additions.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 13/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import ObjectMapper

extension Array
{
    public func isValid(index: Int) -> Bool
    {
        guard self.count > 0 else { return false }
        return index <= (self.count - 1)
    }
    
}
