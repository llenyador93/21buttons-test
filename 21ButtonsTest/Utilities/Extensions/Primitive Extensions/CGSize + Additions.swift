//
//  CGSize + Additions.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 13/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import UIKit

extension CGSize
{
    var ratioW_H : CGFloat {
        
        return self.width/self.height
    }
    
    var sizeType : SizeType {
        
        if      self.height > self.width    { return .heightGreather }
        else if self.height < self.width    { return .widthGreather }
        else                                { return .squared }
        
    }
    
    func size(for maxSize: CGSize) -> CGSize
    {
        switch self.sizeType
        {
        case .heightGreather:
            
            let height  = (self.height > maxSize.height) ? maxSize.height : self.height
            let width   = self.ratioW_H * height
            return CGSize(width: width, height: height)
        
        case .widthGreather:
            
            let width   = (self.width > maxSize.width) ? maxSize.width : self.width
            let height  = width / self.ratioW_H
            return CGSize(width: width, height: height)
            
        case .squared:
            
            let width   = (self.width > maxSize.width) ? maxSize.width : self.width
            return CGSize(width: width, height: width)
        }
    }
}

extension CGSize
{
    enum SizeType
    {
        case heightGreather
        case widthGreather
        case squared
    }
}
