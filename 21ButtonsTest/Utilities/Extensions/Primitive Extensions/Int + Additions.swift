//
//  Int + Additions.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 13/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation

extension Int
{
    var asDisplayableTime : String {
        
        return TimeInterval(self).minuteSecondMS
    }
}
