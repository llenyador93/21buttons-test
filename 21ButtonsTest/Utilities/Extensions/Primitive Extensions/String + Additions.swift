//
//  String + Additions.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 13/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation

extension String
{
    var toURL : URL? {
        return URL(string: self)
    }
}
