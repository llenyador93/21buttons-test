//
//  TimeInterval + Additions.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 14/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation

extension TimeInterval
{
    var minuteSecondMS: String {
        
        return String(format:"%02d:%02d:%02d.%03d", hour, minute, second, millisecond)
    }
    
    var hour: Int {
        return Int(((self/60).truncatingRemainder(dividingBy: 60))/60)
    }
    
    var minute: Int {
        return Int((self/60).truncatingRemainder(dividingBy: 60))
    }
    
    var second: Int {
        return Int(truncatingRemainder(dividingBy: 60))
    }
    
    var millisecond: Int {
        return Int((self*1000).truncatingRemainder(dividingBy: 1000))
    }
}
