//
//  UIColor + Additions.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 10/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import UIKit

public extension UIColor
{
    struct Palette
    {
        public static let turquoise = UIColor(red:0, green:0.68, blue:0.76, alpha:1)
        
        public static let midnightBlue = UIColor(red:0.1, green:0.22, blue:0.36, alpha:1)
        public static let intenseMidnightBlue = UIColor(red:0.1, green:0.22, blue:0.37, alpha:1)
        public static let darkMidnightBlue = UIColor(red:0.16, green:0.36, blue:0.52, alpha:1)
        
        public static let salmon = UIColor(red: 242/255, green: 135/255, blue: 115/255, alpha: 1)
    }
}
