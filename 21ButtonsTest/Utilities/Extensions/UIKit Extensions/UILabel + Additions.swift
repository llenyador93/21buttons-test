//
//  UILabel + Additions.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 10/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation

import UIKit

extension UILabel
{
    func setup(text: String?, textColor: UIColor?, font: UIFont?)
    {
        self.text = text
        self.textColor = textColor ?? .black
        self.font = font ?? UIFont.systemFont(ofSize: 16.0)
    }
}
