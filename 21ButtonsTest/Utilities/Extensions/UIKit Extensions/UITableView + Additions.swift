//
//  UITableView + Additions.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import UIKit

extension UITableView
{
    func registerReusableCell<T: UITableViewCell>(_: T.Type) where T: Reusable
    {
        if let nib = T.nib {
            self.register(nib, forCellReuseIdentifier: T.reuseIdentifier)
        } else {
            self.register(T.self, forCellReuseIdentifier: T.reuseIdentifier)
        }
    }
    
    func dequeueReusableCell<T: UITableViewCell>(indexPath: IndexPath) -> T where T: Reusable
    {
        return self.dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as! T
    }
    
    public func setup(delegateAndDatasource: (UITableViewDelegate & UITableViewDataSource))
    {
        self.delegate   = delegateAndDatasource
        self.dataSource = delegateAndDatasource
    }
    
    func setAutomaticRowHeight(estimatedHeight: CGFloat)
    {
        self.estimatedRowHeight = estimatedHeight
        self.rowHeight = UITableView.automaticDimension
    }
    
    func removeVerticalPadding()
    {
        self.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 0.001))
    }
    
    public func addRefreshControl(with target: Any, and selector: Selector, color: UIColor)
    {
        let refreshFrame = CGRect(x: 0, y: 0, width: self.frame.width, height: 150)
        let refreshControl = UIRefreshControl(frame: refreshFrame)
        refreshControl.backgroundColor = UIColor.clear
        refreshControl.tintColor = color
        refreshControl.addTarget(target, action: selector, for: .valueChanged)
        
        self.refreshControl = refreshControl
    }
}
