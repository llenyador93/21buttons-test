//
//  UIViewController + Alerts.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController
{
    func showErrorAlert(with errorMessage: String)
    {
        let deadline = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        
        DispatchQueue.main.asyncAfter(deadline: deadline)
        {
            self.showAlertWith(title: "Error", message: errorMessage)
        }
    }
    
    func showAlertWith(title: String, message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let actionOk = UIAlertAction(title: "Ok", style: .default, handler: { _ in
        })
        
        alert.addAction(actionOk)
        
        self.present(alert, animated: true, completion: nil)
    }

}
