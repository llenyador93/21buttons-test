//
//  GameTableViewCell.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import UIKit

fileprivate let kMaxImageSize = CGSize(width: 100, height: 100)

protocol GameTableViewCellDisplayLogic
{
    var indexPath           : IndexPath? { get set }
    func display(viewModel  : GameTableViewCellViewModel, indexPath: IndexPath)
    func display(image: UIImage)
}

class GameTableViewCell: UITableViewCell, Reusable
{
    @IBOutlet weak var gameImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    
    var indexPath   : IndexPath?
    
    static var nib: UINib? {
        return UINib(nibName: String(describing: GameTableViewCell.self), bundle: nil)
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        self.setupView()
    }
    
    override func prepareForReuse()
    {
        super.prepareForReuse()
        
        self.gameImageView.image = nil
    }
    
    // MARK: -  Setup methods
    fileprivate func setupView()
    {
        self.backgroundColor = .clear
        self.selectionStyle = .none
        
        self.setupGameImageView()
        self.setupTitleLabel()
    }
    
    fileprivate func setupGameImageView()
    {
        self.gameImageView.backgroundColor = UIColor.gray
    }
    
    fileprivate func setupTitleLabel()
    {
        self.titleLabel.setup(text: "",
                              textColor: UIColor.Palette.midnightBlue,
                              font: nil)
    }
    
    private func setup(imageSize: CGSize)
    {
        let scaledImageSize = imageSize.size(for: kMaxImageSize)
        
        self.imageWidthConstraint.constant = scaledImageSize.width
        self.imageHeightConstraint.constant = scaledImageSize.height
        
        self.setNeedsUpdateConstraints()
    }
}

extension GameTableViewCell : GameTableViewCellDisplayLogic
{
    func display(viewModel: GameTableViewCellViewModel, indexPath: IndexPath)
    {
        self.indexPath = indexPath
        
        self.titleLabel.text = viewModel.title
        
        if let imageSize = viewModel.imageSize
        {
            self.setup(imageSize: imageSize)
        }
    }
    
    func display(image: UIImage)
    {
        self.gameImageView?.image = image
    }
}
