//
//  GameTableViewCellViewModel.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import UIKit

struct GameTableViewCellViewModel
{
    var imageSize   : CGSize?
    var title       : String?
    
    init(imageSize: CGSize?, title: String?)
    {
        self.imageSize  = imageSize
        self.title      = title
    }
}
