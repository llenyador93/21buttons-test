//
//  LoadingTableViewCell.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 10/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

protocol LoadingTableViewCellDisplayLogic
{
    func display(animating: Bool, activityIndicatorColor: UIColor?)
}

class LoadingTableViewCell: UITableViewCell, Reusable
{
    static var nib: UINib? {
        return UINib(nibName: String(describing: LoadingTableViewCell.self), bundle: nil)
    }
    
    @IBOutlet weak var loadingView: NVActivityIndicatorView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.backgroundColor = .clear
        
        self.loadingView.color = UIColor.Palette.turquoise
    }
}

extension LoadingTableViewCell : LoadingTableViewCellDisplayLogic
{
    func display(animating: Bool, activityIndicatorColor: UIColor?)
    {
        animating ? self.loadingView.startAnimating() : self.loadingView.stopAnimating()
        
        let color = activityIndicatorColor ?? UIColor.Palette.turquoise
        self.loadingView.tintColor = color
        self.loadingView.color = color
    }
}
