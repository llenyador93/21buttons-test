//
//  NoResultsTableViewCell.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import UIKit

protocol NoResultsTableViewCellDisplayLogic
{
    func display(viewModel: NoResultsTableViewCellViewModel)
}

class NoResultsTableViewCell: UITableViewCell, Reusable
{
    @IBOutlet weak var noResultsLabel: UILabel!
    
    static var nib: UINib? {
        return UINib(nibName: String(describing: NoResultsTableViewCell.self), bundle: nil)
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
     
        self.setupView()
    }

    // MARK: -  Setup methods
    fileprivate func setupView()
    {
        self.noResultsLabel.setup(text: "",
                                  textColor: UIColor.Palette.midnightBlue,
                                  font: nil)
    }
}

extension NoResultsTableViewCell : NoResultsTableViewCellDisplayLogic
{
    func display(viewModel: NoResultsTableViewCellViewModel)
    {
        self.noResultsLabel.textColor = viewModel.textColor
        self.noResultsLabel.text = viewModel.message
        self.noResultsLabel.textAlignment = viewModel.alignment
    }
}
