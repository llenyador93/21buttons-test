//
//  NoResultsTableViewCellViewModel.swift
//  21ButtonsTest
//
//  Created by Aitor Salvador on 09/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import UIKit

struct NoResultsTableViewCellViewModel
{
    var message     : String
    var alignment   : NSTextAlignment = .center
    var textColor   : UIColor = UIColor.Palette.midnightBlue
}
