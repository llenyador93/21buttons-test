//
//  GamesListDataSourceTests.swift
//  21ButtonsTestTests
//
//  Created by Aitor Salvador on 12/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

@testable import _1ButtonsTest
import XCTest

class GamesListDataSourceTests: XCTestCase
{
    var dataSource  : GamesListDataSource!
    var tableView   : UITableView!
    
    override func setUp()
    {
        self.dataSource = GamesListDataSource()
        self.setupTableView()
    }

    override func tearDown()
    {
        super.tearDown()
    }
    
    fileprivate func setupTableView()
    {
        self.tableView = UITableView(frame: .zero, style: .grouped)
        
        self.tableView.registerReusableCell(LoadingTableViewCell.self)
        self.tableView.registerReusableCell(NoResultsTableViewCell.self)
        self.tableView.registerReusableCell(GameTableViewCell.self)
    }
    
    func testNumberOfCellsLoading()
    {
        self.dataSource.isDownloadLastGamesRquestInProcess = true
        
        XCTAssertEqual(self.dataSource.numberOfRows, 1)
    }
    
    func testNumberOfCellsNoResults()
    {
        self.dataSource.games = []
        
        XCTAssertEqual(self.dataSource.numberOfRows, 1)
    }
    
    func testNumberOfCellsWithSomeResults()
    {
        self.dataSource.games = [Game(), Game(), Game()]
        
        XCTAssertEqual(self.dataSource.numberOfRows, self.dataSource.games.count)
    }
    
    func testCellTypeForCellsLoading()
    {
        self.dataSource.isDownloadLastGamesRquestInProcess = true
        
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = self.dataSource.cell(on: self.tableView, at: indexPath, delegate: self)
        
        XCTAssertTrue(cell is LoadingTableViewCell)
    }
    
    func testCellTypeForNoResults()
    {
        self.dataSource.games = []
        
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = self.dataSource.cell(on: self.tableView, at: indexPath, delegate: self)
        
        XCTAssertTrue(cell is NoResultsTableViewCell)
    }
    
    func testCellTypeForSomeResults()
    {
        self.dataSource.games = [Game(), Game(), Game()]
        
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = self.dataSource.cell(on: self.tableView, at: indexPath, delegate: self)
        
        XCTAssertTrue(cell is GameTableViewCell)
    }
    
    func testCellTypeForSomeResultsLoading()
    {
        self.dataSource.games = [Game(), Game(), Game()]
        self.dataSource.isDownloadLastGamesRquestInProcess = true
        
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = self.dataSource.cell(on: self.tableView, at: indexPath, delegate: self)
        
        XCTAssertTrue(cell is GameTableViewCell)
    }
}
