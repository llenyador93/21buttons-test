//
//  GamesListViewControllerTests.swift
//  21ButtonsTestTests
//
//  Created by Aitor Salvador on 15/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

@testable import _1ButtonsTest
import Foundation
import RxSwift
import XCTest

class GamesListViewControllerTests: XCTestCase
{
    // MARK: Subject under test
    
    var sut             : GamesListViewController!
    var interactorSpy   : GamesListBusinessLogicSpy!
    var routerSpy       : GamesListRoutingLogicSpy!
    var window          : UIWindow!
    
    // MARK: Test lifecycle
    
    override func setUp()
    {
        super.setUp()
        window = UIWindow()
        setupGamesListViewController()
        setupGamesListInteractor()
    }
    
    override func tearDown()
    {
        window = nil
        super.tearDown()
    }
    
    // MARK: Test setup
    
    func setupGamesListViewController()
    {
        sut = GamesListViewController(nibName: nil, bundle: nil)
    }
    
    func setupGamesListRouter()
    {
        routerSpy = GamesListRoutingLogicSpy()
        sut.router = routerSpy
    }
    
    func setupGamesListInteractor()
    {
        interactorSpy = GamesListBusinessLogicSpy()
        sut.interactor = interactorSpy
    }
    
    func loadView()
    {
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }
    
    // MARK: - Tests
    
    // MARK: Number of sections tests
    func testNumberOfRowsWithGamesList()
    {
        // Given
        let tableView = self.sut.tableView
        let indexPath0 = IndexPath(row: 0, section: 0)
        
        guard let game = GameStub.game else { XCTFail(); return }
        self.sut.dataSource.games = [game, game, game]
        
        // When
        self.loadView()
        
        let rows = self.sut.tableView(tableView, numberOfRowsInSection: 0)
        let cell = self.sut.tableView(tableView, cellForRowAt: indexPath0)

        // Then
        XCTAssertEqual(rows, 3)
        XCTAssertTrue(cell is GameTableViewCell)
    }
    
    func testNumberOfRowsWithoutGamesList()
    {
        // Given
        let tableView = self.sut.tableView
        let indexPath0 = IndexPath(row: 0, section: 0)
        
        self.sut.dataSource.games = []
        
        // When
        self.loadView()
        let rows = self.sut.tableView(tableView, numberOfRowsInSection: 0)
        let cell = self.sut.tableView(tableView, cellForRowAt: indexPath0)
        
        // Then
        XCTAssertEqual(rows, 1)
        XCTAssertTrue(cell is NoResultsTableViewCell)
    }
    
    func testNumberOfRowsWhileLoading()
    {
        // Given
        let tableView = self.sut.tableView
        let indexPath0 = IndexPath(row: 0, section: 0)
        
        self.sut.dataSource.games = []
        self.sut.dataSource.isDownloadLastGamesRquestInProcess = true

        // When
        self.loadView()
        let rows = self.sut.tableView(tableView, numberOfRowsInSection: 0)
        let cell = self.sut.tableView(tableView, cellForRowAt: indexPath0)
        
        // Then
        XCTAssertEqual(rows, 1)
        XCTAssertTrue(cell is LoadingTableViewCell)
    }
    
    func testDownloadLastGamesCalled()
    {
        self.loadView()
        
        XCTAssertTrue(self.interactorSpy.isDownloadLastGamesListCalled)
    }
    
    func testSelectGameIsCalled()
    {
        // Given
        let tableView = self.sut.tableView
        let indexPath0 = IndexPath(row: 0, section: 0)
        
        self.sut.dataSource.games = []
        self.sut.dataSource.isDownloadLastGamesRquestInProcess = true
        
        // When
        self.loadView()
        self.sut.tableView(tableView, didSelectRowAt: indexPath0)
        
        // Then
        XCTAssertTrue(self.interactorSpy.isSelectGameCalled)
    }
    
    func testDisplayImageIsCalled()
    {
        // Given
        let tableView = self.sut.tableView
        let indexPath0 = IndexPath(row: 0, section: 0)
        
        self.sut.dataSource.games = []
        
        // When
        self.loadView()
        _ = self.sut.tableView(tableView, cellForRowAt: indexPath0)
        
        // Then
        XCTAssertTrue(self.interactorSpy.isDisplayImageForCellCalled)
    }
    
    // MARK: - Routing
    func testRouteToGameDetailCalled()
    {
        // Given
        setupGamesListRouter()
        
        // When
        self.sut.displaySelectedGame()
        
        // Then
        XCTAssertTrue(self.routerSpy.isRouteToSelectedGameDetailCalled)
    }
}


