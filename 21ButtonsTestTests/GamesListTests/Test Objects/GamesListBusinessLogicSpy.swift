//
//  GamesListBusinessLogicSpy.swift
//  21ButtonsTestTests
//
//  Created by Aitor Salvador on 15/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
import UIKit
@testable import _1ButtonsTest

class GamesListBusinessLogicSpy : GamesListBusinessLogic
{
    var isDownloadLastGamesListCalled = false
    func downloadLastGamesList()
    {
        isDownloadLastGamesListCalled = true
    }
    
    var isSelectGameCalled = false
    func selectGame(at indexPath: IndexPath)
    {
        isSelectGameCalled = true
    }
    
    var isDisplayImageForCellCalled = false
    func displayImage(at indexPath: IndexPath, on cell: UITableViewCell)
    {
        isDisplayImageForCellCalled = true
    }
}
