//
//  GamesListDisplayLogicSpy.swift
//  21ButtonsTestTests
//
//  Created by Aitor Salvador on 15/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
@testable import _1ButtonsTest

class GamesListDisplayLogicSpy: GamesListDisplayLogic
{
    var isDisplaySelectedGameCalled = false
    func displaySelectedGame()
    {
        self.isDisplaySelectedGameCalled = true
    }
    
    var isDisplayErrorMessageCalled = false
    func display(errorMessage: String)
    {
        self.isDisplayErrorMessageCalled = true
    }
    
    var isDisplayDataSourceCalled = false
    func display(dataSource: GamesListDataSource)
    {
        self.isDisplayDataSourceCalled = true
    }
}
