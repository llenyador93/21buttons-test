//
//  GamesListPresentationLogicSpy.swift
//  21ButtonsTestTests
//
//  Created by Aitor Salvador on 15/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
@testable import _1ButtonsTest

class GamesListPresentationLogicSpy: GamesListPresentationLogic
{
    var isPresentDataSourceCalled = false
    func present(dataSource: GamesListDataSource)
    {
        isPresentDataSourceCalled = true
    }
    
    var isPresentSelectedGameCalled = false
    func presentSelectedGame()
    {
        isPresentSelectedGameCalled = true
    }
    
    var isPresentErrorCalled = false
    func present(error: Error)
    {
        self.isPresentErrorCalled = true
    }
}
