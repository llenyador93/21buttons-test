//
//  GamesListRoutingLogic.swift
//  21ButtonsTestTests
//
//  Created by Aitor Salvador on 15/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

@testable import _1ButtonsTest
import XCTest

class GamesListRoutingLogicSpy : NSObject, GamesListRoutingLogic, GamesListDataPassing
{
    var dataStore: GamesListDataStore?
    
    var isRouteToSelectedGameDetailCalled = false
    func routeToSelectedGameDetail()
    {
        self.isRouteToSelectedGameDetailCalled = true
    }
}
