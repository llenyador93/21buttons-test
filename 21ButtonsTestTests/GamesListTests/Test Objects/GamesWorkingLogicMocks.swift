//
//  GamesWorkingLogicMocks.swift
//  21ButtonsTestTests
//
//  Created by Aitor Salvador on 15/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
@testable import _1ButtonsTest
import RxSwift

class GamesWorkerVoidListMock : GamesWorkingLogic
{
    func downloadLastGamesList() -> Single<[Game]>
    {
        return Single.just([])
    }
}

class GamesWorkerErrorMock : GamesWorkingLogic
{
    func downloadLastGamesList() -> Single<[Game]>
    {
        return Single.error(StandardError.unknown)
    }
}
