//
//  GamesWorkingLogicSpy.swift
//  21ButtonsTestTests
//
//  Created by Aitor Salvador on 15/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
@testable import _1ButtonsTest
import RxSwift

class GamesWorkingLogicSpy : GamesWorkingLogic
{
    var isGetLastGamesCalled = false
    
    func downloadLastGamesList() -> Single<[Game]>
    {
        isGetLastGamesCalled = true
        return Single.just([])
    }
}
