//
//  GameStub.swift
//  21ButtonsTestTests
//
//  Created by Aitor Salvador on 15/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation
@testable import _1ButtonsTest
import ObjectMapper

struct GameStub
{
    static var game : Game?
    {
        if let jsonDict = FileJSONReader.json(forFilename: "GameData")
        {
            return Mapper<Game>().map(JSON: jsonDict)
        }
        return nil
    }
}
