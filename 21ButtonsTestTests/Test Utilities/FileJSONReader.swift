//
//  FileJSONReader.swift
//  21ButtonsTestTests
//
//  Created by Aitor Salvador on 15/01/2019.
//  Copyright © 2019 Aitor Salvador. All rights reserved.
//

import Foundation

class FileJSONReader
{
    static func json(forFilename filename: String) -> [String : Any]?
    {
        do
        {
            let file = Bundle(for: self).url(forResource: filename, withExtension: "json")!
            
            let data = try Data(contentsOf: file)
            
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
            return jsonResult as? [String : Any]
        }
            
        catch
        {
            print(error)
        }
        
        return nil
    }
}
